# Powershell
ls - vypíše seznam položek ve složce
cd - vstoupí do složky
mkdir - vytvoří složku 
cd .. - vrátí se do o stupeň výš
code . otevře visual studio code (rovnou tu určitou složku)


git              základní příkaz pro vše týkající se gitu
git init         inicializuje git ve složce, vytvoří soubor .git
git add          přidá soubor či složku do sledovaných souborů
git add .        přidá celý obsah složky do sledovaných položek
git status       ukáže stav složky, jaké soubory jsou sledované a                   jaké ne, zda je složka ve up-to-date
git commit -m    změny sledovaný souboru ve složce připraví na                      odeslání na cloud, zpráva po -m by měla popisovat                  změny
git remote add origin   přidá git repozitář, do kterého se data                     posílají
git push         odešle všechny commity na cloud
git pull         stáhne commity z cloudu